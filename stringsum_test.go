package main

import (
	"testing"

	"github.com/magiconair/properties/assert"
)

func TestGivenEmptyStringWhenSumIsCalledThenShouldReturnZero(t *testing.T){
	input := ""
	result := Sum(input, input)
	assert.Equal(t, 0, result)
}

func TestGivenANaturalNumberAndAnEmptyStringWhenSumIsCalledThenShouldReturnTheNaturalNumber(t *testing.T) {
	natualNumber := "1"
	emptyString := ""

	result := Sum(natualNumber, emptyString)

	assert.Equal(t, 1, result)
}