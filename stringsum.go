package main

import "strconv"

func Sum(number1, number2 string) int {
	firstNumber, secondNumber := 0, 0
	if number1 != ""{
		firstNumber, _ = strconv.Atoi(number1)
	}
	if number2 != "" {
		secondNumber, _ = strconv.Atoi(number2)
	}
	return firstNumber + secondNumber
}